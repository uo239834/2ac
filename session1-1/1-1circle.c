#include "1-1circle.h"
#include <stdio.h>

double circleArea(double radius)
{
const double PI= 3.1415;
return PI *radius*radius;
}
